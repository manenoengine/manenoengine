#labels Phase-Deploy,Phase-Design
The Maneno Engine is an English-Swahili translator and etymology tool

= Introduction =

Kiswahli is spoken by millions of people in East Central and Southern Africa. It is an official language in Kenya and the East African Community. It is one of the official languages of the African Union and is now proposed to be one of the languages used at the United Nations. Considering this impressive stature of the language, it is sad that there is hardly any Kiswahili language tools to speak of. The first open source Kiswahili spell-checker was created in 2004 by Jason Githeko and Dwayne Bailey and later, an enhanced version was incorporated into OpenOffice. Nothing has been done since that time to provide Swahili language tools.

This project seeks to contribute to the effort to provide Kiswahili (the correct name for the language is Kiswahili) with a respectable digital presence.


= Details =

The current application is a Java tool that runs on any platform where a Java Virtual Machine is installed. To run it:
<ul>
<li> Download the zip file and uncompress with tools such as gzip, peazip,  7zip or The Unarchiver.</li>
<li>If you have Java Webstart installed open the "launch.jnlp" file to start the application else launch the .jar file (Maneno031.jar).</li>
<li> The interface is fairly simple. In General, only single English or Kiswahili words can be translated. There are few exceptions. Remove any trailing white space from each word.</li>
</ul>

Future versions are planned to:
<ul>
<li>Expand the _vocabulary_ by harvesting from *high quality* translations</li>
<li>Add descriptors to each word</li>
<li>Include examples of usage</li>
<li>Include the history of each word where available</li>
<li>Eventually include an audio pronunciation guide</li>
</ul>
